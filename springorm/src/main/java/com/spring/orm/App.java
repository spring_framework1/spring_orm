package com.spring.orm;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.spring.orm.dao.StudentDao;
import com.spring.orm.entities.Student;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       ApplicationContext context = new ClassPathXmlApplicationContext("config.xml");
       StudentDao studentDao = context.getBean("studentDao",StudentDao.class);
       
       /* Using this code only we are going to create the console based application so we commented this part
        
       Student student = new Student(101, "Raviraj Navale", "Pune");
       int insert = studentDao.insert(student);
       System.out.println("Done:)"+insert);
       
       */
       
       
       BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
       boolean check = true;
       while(check)
       {
    	   System.out.println("==============================================================================================");
           System.out.println("Please Choose from Menu");
           System.out.println();
           System.out.println("1. Add New Student");
           System.out.println("2. Display All Student");
           System.out.println("3. Get Details of One Student");
           System.out.println("4. Delete Studenet");
           System.out.println("5. Update Student");
           System.out.println("6. Exit");
    	   System.out.println("==============================================================================================");
           
           
           try {
        	   
        	  int input = Integer.parseInt(br.readLine());
        	  
        	  switch(input) {
        	  
        	  case 1:
        		  //Add Student
        		  
        		  //taking inputs from user
        		  System.out.println("Enter Student ID: ");
        		  int sId = Integer.parseInt(br.readLine());
        		  System.out.println("Enter Student Name: ");
        		  String sName = br.readLine();
        		  System.out.println("Enter Student City: ");
        		  String sCity = br.readLine();
        		  
        		  //creating object and setting all the values
        		  Student student = new Student();
        		  student.setStudentId(sId);
        		  student.setStudentName(sName);
        		  student.setStudentCity(sCity);
        		  
        		  //calling insert method and saving it into database
        		  int insert = studentDao.insert(student);  // here insert method will return the ID of added Student
        		  System.out.println("--------------------------------------------------------------------------------------");

        		  System.out.println(insert+" Student Added Successfully:) "); 
        		  System.out.println();
        		  break;
        		  
        	  case 2:
        		  //Display All Student
        		  
        		  List<Student> allStudents = studentDao.getAllStudent();
        		  System.out.println("--------------------------------------------------------------------------------------");
        		  System.out.println("List of All students: ");
        		  //System.out.println(allStudents); // As we have added constructor we can print by this also
        		  
        		  // By using for each loop we can print the data
        		  for(Student s:allStudents)
        		  {
        			  System.out.println("ID: "+s.getStudentId()+" Name: "+s.getStudentName()+" City: "+s.getStudentCity());
        		  }
        		  
        		  
        		  System.out.println("--------------------------------------------------------------------------------------");

        		  break;
        		  
        	  case 3:
        		  //Get Detail of One Student
        		  
        		  System.out.println("Enter Student ID: ");
        		  int sId1 = Integer.parseInt(br.readLine());
        		  Student student1 = studentDao.getStudent(sId1);
        		  System.out.println("--------------------------------------------------------------------------------------");
        		  System.out.println("Student Information: ");
        		  System.out.println(student1);
        		  
        		  /* By using for each lopp also we can print the data
        		  for(Student s:student1)
        		  {
        			  System.out.println("ID: "+s.getStudentId()+" Name: "+s.getStudentName()+" City: "+s.getStudentCity());
        		  }
        		  */
        		  System.out.println("--------------------------------------------------------------------------------------");
        		  
        		  break;
        		  
        	  case 4:
        		  //Delete Student
        		  
        		  System.out.println("Enter Student ID: ");
        		  int sId2 = Integer.parseInt(br.readLine());
        		  studentDao.delete(sId2);
        		  System.out.println("--------------------------------------------------------------------------------------");
        		  System.out.println("Student Deleted with ID: "+sId2);
        		  System.out.println("--------------------------------------------------------------------------------------");
        		  
        		  break;
        		  
        	  case 5:
        		  //Update Student
        		  
        		  

        		  break;
        		  
        	  case 6:
        		  //Exit
        		  check = false;
        		  break;	  
        		  
        	  
        	  }
			
		} catch (Exception e) {
			System.out.println("Invalid Input Try with Another");
			System.out.println(e.getMessage());
		}
       }
       
       System.out.println("See you soon :)");

       
       
    }
}
